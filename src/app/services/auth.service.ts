import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Usuario } from '../interfaces/usuario.interface';
import { HttpClient } from '@angular/common/http';
import { UsuarioModel } from '../models/usuario.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private API_URL = environment.rest_api_server;
  userToken: string;

  constructor(private http: HttpClient) {
    this.readToken();
  }

  login(usuario: UsuarioModel) {

    return this.http.post(`${this.API_URL}/api/v1/sistema/login`, usuario)
    .pipe(
      map( resp => {
        // tslint:disable-next-line: no-string-literal
        this.saveToken( resp['token'] );
        return resp;
      })
    );

  }

  private saveToken(token: string) {
    this.userToken = token;
    localStorage.setItem('token', token);
  }

  readToken() {
    if (localStorage.getItem('token')) {
      this.userToken = localStorage.getItem('token');
    } else {
      this.userToken = '';
    }
    return this.userToken;
  }

}
