import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import {  throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';

const API_URL = environment.rest_api_server;

@Injectable({
  providedIn: 'root'
})
export class PerfilesService {

  constructor( private http: HttpClient ) { }

  public getAll() {
    return this.http.get( API_URL + '/api/v1/perfiles/getall' ).pipe(catchError(this.handleError));
    // return this.httpClient.get(this.REST_API_SERVER).pipe(retry(3), catchError(this.handleError));
  }

  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    Swal.fire({
      icon: 'error',
      title: 'Error',
      text: errorMessage,
    });
    return throwError(errorMessage);
  }
}
