export class UsuarioModel {
    id: string;
    uuid: string;
    nombre: string;
    matricula: string;
    password: string;
    permisos: [];
    perfilName: string;
    isSuper: boolean;
    activo: boolean;
    perfil: {};
}
