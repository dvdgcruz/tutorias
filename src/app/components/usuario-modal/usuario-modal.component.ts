import { Component, Inject, OnInit } from '@angular/core';
import {  MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-usuario-modal',
  templateUrl: './usuario-modal.component.html',
  styleUrls: ['./usuario-modal.component.css']
})
export class UsuarioModalComponent {


  constructor(public dialogRef: MatDialogRef<UsuarioModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any ) {
                console.log( data );
               }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
