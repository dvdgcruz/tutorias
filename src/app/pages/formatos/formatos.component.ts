import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Usuario } from 'src/app/interfaces/usuario.interface';
import { takeUntil } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { PerfilesService } from 'src/app/services/perfiles.service';
import { UsuariosService } from 'src/app/services/usuarios.service';
import { Subject } from 'rxjs';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-formatos',
  templateUrl: './formatos.component.html',
  styleUrls: ['./formatos.component.css']
})
export class FormatosComponent implements OnInit {

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  displayedColumns: string[] = ['matricula', 'nombre', 'perfilName'];
  dataSource: MatTableDataSource<Usuario>;
  destroy$: Subject<boolean> = new Subject<boolean>();
  usuarios = [];
  perfiles: any = [];
 // usuario = new UsuarioModel();
  cargando = true;
  elementoselect: any;

  constructor( private usuariosService: UsuariosService,
               public dialog: MatDialog,
               private perfilesService: PerfilesService ) { }

  ngOnInit(): void {
    this.usuariosService.getAll().pipe(takeUntil(this.destroy$)).subscribe( ( resp: Usuario[] ) => {
      this.cargando = false;
      this.usuarios = resp;
      this.dataSource = new MatTableDataSource<Usuario>( this.usuarios );
    } );
    this.perfilesService.getAll().subscribe( resp => {
      this.perfiles = resp;
    });
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngOnDestroy() {
    this.destroy$.next(true);
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
