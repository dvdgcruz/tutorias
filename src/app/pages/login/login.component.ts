import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { UsuarioModel } from 'src/app/models/usuario.model';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  usuario: UsuarioModel = new UsuarioModel();

  constructor( private auth: AuthService, private router: Router ) { }

  ngOnInit(): void {
  }

  login( form: NgForm ) {
    if ( form.invalid ) { return; }

    Swal.fire({
      allowOutsideClick: false,
      icon: 'info',
      text: 'Espere un momento por favor'
    });
    Swal.showLoading();

    this.auth.login( this.usuario )
    .subscribe( async resp => {
      console.log( resp );
      await Swal.close();
      this.router.navigateByUrl('/dashboard');
    }, (err) => {
      Swal.fire({
        icon: 'error',
        title: 'Error al autenticar',
        // text: 'ERRRROOOOORRRRR'
      });
    });
  }

}
