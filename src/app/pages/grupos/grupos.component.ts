import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Subject } from 'rxjs';
import { UsuariosService } from 'src/app/services/usuarios.service';
import { PerfilesService } from 'src/app/services/perfiles.service';
import { MatDialog } from '@angular/material/dialog';
import { takeUntil } from 'rxjs/operators';
import { Usuario } from 'src/app/interfaces/usuario.interface';
import { UsuarioModel } from 'src/app/models/usuario.model';
import { UsuarioModalComponent } from 'src/app/components/usuario-modal/usuario-modal.component';

@Component({
  selector: 'app-grupos',
  templateUrl: './grupos.component.html',
  styleUrls: ['./grupos.component.css']
})
export class GruposComponent implements OnInit {
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  displayedColumns: string[] = ['matricula', 'nombre', 'perfilName'];
  dataSource: MatTableDataSource<Usuario>;
  destroy$: Subject<boolean> = new Subject<boolean>();
  usuarios = [];
  perfiles: any = [];
  usuario = new UsuarioModel();
  cargando = true;

  constructor(
    private usuariosService: UsuariosService,
    public dialog: MatDialog,
    private perfilesService: PerfilesService 
  ) { }

  ngOnInit(): void {
    this.usuariosService.getAll().pipe(takeUntil(this.destroy$)).subscribe( ( resp: Usuario[] ) => {
      this.cargando = false;
      this.usuarios = resp;
      this.dataSource = new MatTableDataSource<Usuario>( this.usuarios );
    } );
    this.perfilesService.getAll().subscribe( resp => {
      this.perfiles = resp;
    });
  }
  // tslint:disable-next-line:use-lifecycle-interface
  ngOnDestroy() {
    this.destroy$.next(true);
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  openDialog(): void {
    const dialogRef = this.dialog.open( UsuarioModalComponent, {
      width: '230px',
      data: {usuario: this.usuario, perfiles: this.perfiles},
    });

    dialogRef.afterClosed().subscribe( result => {
      console.log('The dialog was closed', result.usuario);
      if ( result !== undefined ) {
        this.usuario = result.usuario;
        this.usuario.perfilName = result.usuario.perfil.nombre;
        delete this.usuario.perfil;
        this.usuariosService.saveOne( this.usuario ).subscribe( resp => {
          this.usuarios.push(resp);
        } );
      }
    });
  }

}
