import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditformatosComponent } from './editformatos.component';

describe('EditformatosComponent', () => {
  let component: EditformatosComponent;
  let fixture: ComponentFixture<EditformatosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditformatosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditformatosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
